const { authenticated, authorized } = require("../Auth/auth");
const bcrypt = require("bcryptjs");
const { UserInputError, GraphQLUpload } = require("apollo-server");
const axios = require('axios');

const lockotp={
otp:"",
}
const generateotp=()=>{
  var otp = Math.floor(1000 + Math.random() * 9000);
  return otp;
}

const {
  validateLoginInput,
  validateRegisterInput,
} = require("../Auth/validate");

module.exports = {
  Query: {
    getUsers: async (_, __, { User }) => {
      const users = await User.find();
      return users;
    },
  },
  Mutation: {
    otpVerify:async(_,{otp,mobile},{User,createToken})=>{
         console.log(otp +"otp");
         console.log(lockotp.otp);
        if(otp==lockotp.otp){
          const user = await User.findOne({ mobile:mobile });
          const token = createToken(user);
        return { token, user };
        }
        else{
          throw new Error("otp not matched");
        }
      
    },

    signUp: async (_, { input }, { User, createToken }) => {
      try {
        console.log(input);
        const existing = await User.findOne({ email: input.email });
     
        if (existing) {
          throw new Error("User Exists");
        }

        const role =
          input.email === "shabab.23rhythm@gmail.com" ? "ADMIN" : "MEMBER";
        const hashed = await bcrypt.hash(input.password, 12);
        const { valid, errors } = validateRegisterInput(
          input.name,
          input.email,
          input.password,
          input.confirmPassword,
          input.mobile,
        );
        if (!valid) {
          throw new UserInputError("Errors", { errors });
        }
        input.password = hashed;
        const newUser = new User({ ...input, role });
        console.log("======", newUser.password);
        const result = await newUser.save();

        const token = createToken(newUser);
        return { token, user: result };
      } catch (error) {
        console.log(error);
        throw error;
      }
    },
    signIn: async (_, { input }, { User, createToken }) => {
      console.log(input);
    let user={};
      if(input.email){

       user = await User.findOne({ email: input.email });
      }
      else if(input.mobile){
        
        user = await User.findOne({ mobile: input.mobile });
        const otp=generateotp();
        
        lockotp.otp=otp;
        const mobile=input.mobile;
        const content=`please verify your account, your otp is${otp}`;
        axios.post('http://gosms.xyz/api/v1/sendSms',{username:"medylife",
        password:"Vu3wq8e7j7KqqQN",
        sms_content:`Your verification code is ${otp}`,
        number:input.mobile,
        sms_type:1,
        masking:"non-masking"
      },{
          headers: {
            
            'Content-Type': 'application/json',
        }
        })
        .then(response => {
         
                        console.log(response.data)         })
              .catch(error => {
                
                   });
                 
                   
        console.log(lockotp.otp);
        return{token:"not verified",user};
      }
         console.log(user);
      if (!user) {
        throw new Error("User not found");
      }
      const token = createToken(user);
      return { token, user };
    },
  },
};
