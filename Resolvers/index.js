const userResolvers = require("./user");
const productResolvers = require("./product");
const categoryResolvers = require("./category");
const cartResolvers =require("./cart")
const { GraphQLDateTime } = require("graphql-iso-date");

const cusDateScalarResolver = {
  Date: GraphQLDateTime,
};

module.exports = [
  cartResolvers,
  userResolvers,
  cusDateScalarResolver,
  productResolvers,
  categoryResolvers,
];
