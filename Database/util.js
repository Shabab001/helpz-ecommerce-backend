const mongoose = require("mongoose");


const connectionUri =process.env.MONGODB_URI||'mongodb+srv://shabab:p12345@cluster0.xopis.mongodb.net/helpz?retryWrites=true&w=majority';
module.exports.connection = async () => {
  try {
   
    mongoose.set("debug", true);
    module.exports.conn=await mongoose.connect(connectionUri , {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log("Database connected succesfully");
  } catch (error) {
    console.error(error);
    throw error;
  }
};
