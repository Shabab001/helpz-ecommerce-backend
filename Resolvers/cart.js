const { authenticated, authorized } = require("../Auth/auth");
const Cart = require("../Models/Cart");
const {v4 : uuidv4} = require('uuid')


module.exports = {
    Query: {
      getCarts:authenticated( async (_, __, { Category }) => {
        const cart = await Cart.find();
        if(cart){
          
          return cart;
        }
      }),
  
    //   findCat: async (_, { name }, { Category }) => {
    //     console.log(name);
    //     const cat = await Category.find({ name: name }).exec();
    //     return cat;
    //   },
    },
    Mutation: {
      createCart: authenticated(async (_, {input}, { Cart,Product,User }) => {
          console.log("in cart")
        console.log(input);
        
        const{products,user,TotalPrice}=input
        const cart= new Cart(
           
            {_id:uuidv4(),
              user:user,
              TotalPrice:TotalPrice,
             
              cartItems:products
            
            },
            {
              upsert: true,
              new: true,
            }
          )
          console.log(cart);
          const result = await cart.save();
            console.log(result);
       return result
      }),
    },
    Cart: {
      user: async (data, _, { User}) => {
        try {
          const user = await User.findById({_id:data.user });
          return user;
        } catch (error) {
          console.log(error);
          throw error;
        }
      },
    },
    productsDetails: {
      product: async (data, _, { Product }) => {
         console.log("hi")
        console.log(data.product);
        try {
          const product = await Product.findById({_id:data.product });
          console.log(product +"product")
          if(product){

            return product;
          }
        } catch (error) {
          console.log(error);
          throw error;
        }
      },
    },
  };