const { gql } = require("apollo-server");
module.exports = gql`
  extend type Query {
    getCarts: [Cart!]
  }
  extend type Mutation {
    createCart(input: createCartInput!): Cart!
    
  }
  input createCartInput {
    products: [productInput!]
    user: String!
    TotalPrice:Int!
  }
  input productInput {
    product:String!
    productPrice:Int!
    quantity:Int!
  }

  type Cart {
    id: ID
    cartItems: [productsDetails!]
    user: User
    TotalPrice:Int
  }
  type productsDetails{
    product:Product!
    productPrice:Int!
    quantity:Int!

  }
`;
