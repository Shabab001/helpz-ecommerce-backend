const { Schema, model } = require("mongoose");

const userSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    
    },
    price: {
      type: Number,
      required: true,
    },
    brand: {
      type: String,
      required: true,
    },
    media: [
      {
        type: String,
        
      },
    ],
    description:{
      type:String,
      trim:true,
    },
    Text:[{
      type:String,
     
    }],
    icon:[
      {img:{
        type:String}} 
    ],
    reviews:[
      {
        userId:{type:Schema.Types.ObjectId,
        ref:"User"
        },
        review:String,
      }
    ],

    subCategory:[
      {
        sub:String
      }
    ],
    offer:{
       type:Number
    },
    special:[{
      rewards:String

    }
    ],
    
    category: [
      {
        type: Schema.Types.ObjectId,
        ref: "Category",
      },
    ],
  },
  {
    timestamps: true,
  }
);
module.exports = model("Product", userSchema);
