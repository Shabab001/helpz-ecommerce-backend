const { Schema, model } = require("mongoose");
const { schema } = require("./user");

const cartSchema = new Schema({
    _id:{type:String},
    user: { type:Schema.Types.ObjectId, ref: 'User', required: true },
    cartItems: [
        {   
            
            product: { type:Schema.Types.ObjectId, ref: 'Product', required: true },
            quantity: { type: Number, require:true },
            productPrice: { type: Number, required: true }
        },
    ],
        TotalPrice:{
            type:Number,
            require:true
        }
}, { timestamps: true });


module.exports =model('Cart', cartSchema);