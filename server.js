const express = require("express");
const { ApolloServer } = require("apollo-server-express");
const { connection,conn } = require("./Database/util");
const app = express();
const typeDefs = require("./TypeDefs");
const resolvers = require("./Resolvers");
const Dotenv = require("dotenv");
const { createToken, getUserFromToken } = require("./Auth/auth");
const User = require("./Models/user");
const Cart = require("./Models/Cart");
const Product = require("./Models/Product");
const Category = require("./Models/Category");
const cors = require("cors");
const multer = require("multer");
const AdminBroMongoose = require('@admin-bro/mongoose')

const AdminBro = require('admin-bro');
const AdminBroExpress = require('@admin-bro/express');

const stripe = require("stripe")(
  "sk_test_51H1sYLGvsYapsm2yRuJCRj6BdRMyZgw8fK6Pj6ZMiqUryGpCZz18pbxBDM6TGt6m8yehnVEcMkRmhYIH5Rl0MLkF00ZCNHcGJz"
);
const { existsSync, mkdirSync } = require("fs");
const productRoutes = require('./router/product.js');
Dotenv.config();
connection();
console.log("hi");
app.use(express.json());
app.use(cors());
app.use("/images", express.static("images"));




const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req }) => {
    const token = req.headers.authorization;

    const userT = await getUserFromToken(token);

    return { User, Product, Category, Cart, userT, createToken };
  },
  debug: true,
  tracing: true,
  introspection: true,
  playground: true
});
//server
const port = process.env.PORT || 5000
server.applyMiddleware({ app });
app.listen(port, res => {
  console.log(`Server is running at ${port}`);
});

//admin bro
AdminBro.registerAdapter(AdminBroMongoose);
const adminBro = new AdminBro({
  resources: [Product,User,Category,Cart],
  rootPath: '/admin',
})

const router = AdminBroExpress.buildRouter(adminBro);
app.use(adminBro.options.rootPath, router);

//payment
app.post("/payment", (req, res) => {
  const { product, token, price } = req.body;
  console.log(product, token, price);

  const des = product.map(prod => prod.name);
  return stripe.customers
    .create({
      email: token.email,
      source: token.id,
    })
    .then(customer => {
      stripe.charges.create({
        amount: price * 100,
        currency: "usd",
        customer: customer.id,
        description: des.toString(),
      });
    })
    .then(result => res.status(200).json(result))
    .catch(err => conole.log(err));
});

//image 
// var storage = multer.diskStorage({
//   destination: (req, file, cb) => {
//     cb(null, "images/");
//   },
//   filename: (req, file, cb) => {
//     cb(null, `${file.originalname.replace(/\s/g, "-")}`);
//   },
//   fileFilter: (req, file, cb) => {
//     const ext = path.extname(file.originalname);
//     if (ext !== ".jpg" || ext !== ".png") {
//       return cb(res.status(400).end("only jpg, png are allowed"), false);
//     }
//     cb(null, true);
//   },
// });
// let upload = multer({ storage: storage }).single("file");

// app.post("/product/uploadImage", (req, res) => {
//   console.log("HERE");

//   upload(req, res, err => {
//     console.log(res.req.file);
//     console.log("going");
//     if (err) return res.json({ success: false, err });
//     return res.json({
//       success: true,
//       image: res.req.file.path,
//       fileName: res.req.file.filename,
//     });
//   });
// });


// Routes

// product routes
app.use('/api',productRoutes)
app.get('/', (req,res)=>{
res.send({messege:"hi"})
})