const Category = require("../Models/Category");

function createCategories(categories, parentId = null) {
    const categoryList = [];
    let category;
    if (parentId == null) {
      category = categories.filter((cat) => cat.parentId == undefined);
    } else {
      category = categories.filter((cat) => cat.parentId == parentId);
    }
 
    for (let cate of category) {
         
      categoryList.push({
        id: cate.id,
        name: cate.name,
    
        parentId: cate.parentId,
     
        subCategories: createCategories(categories, cate.id),
      });
    }
    
    console.log(categoryList);
    return categoryList;
  }

  module.exports=  createCategories ;